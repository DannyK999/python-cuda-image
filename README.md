# Python CUDA Image

This is an extremely basic CI repo to generate a variant of the python 3.8 image with CUDA 11.4 (Update 3).
Other combinations may be served her in future.

The purpose of such image is to as closely resemble the existing python image but with the capability to use CUDA "autodetect" packages in CI builds.
