FROM python:3.8-buster

ENV DEBIAN_FRONTEND noninteractive

RUN wget -q https://developer.download.nvidia.com/compute/cuda/11.4.3/local_installers/cuda-repo-debian10-11-4-local_11.4.3-470.82.01-1_amd64.deb \
    && dpkg -i cuda-repo-debian10-11-4-local_11.4.3-470.82.01-1_amd64.deb \
    && apt-key add /var/cuda-repo-debian10-11-4-local/7fa2af80.pub \
    && apt-get update \
    && apt-get -y install --no-install-recommends software-properties-common \
    && add-apt-repository contrib \
    && apt-get update \
    && apt-get -y install --no-install-recommends cuda \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && rm cuda-repo-debian*.deb

RUN echo "export PATH=/usr/local/cuda-11.4/bin:/opt/nvidia/nsight-compute/${PATH:+:${PATH}}" >> ~/.bashrc
